#!/bin/python3

# BEGIN PROGRAM
import re
import urllib.request
import urllib.error
from html.parser import HTMLParser
import shutil
import os
import sys
import time

gal_match = r'https\:\/\/t\.nhentai\.net\/galleries\/([0-9]+)\/([0-9]+)t\.([jpg|png]+)'
thumb_match = r'[0-9]+t\.jpg'
pages_match = r'([0-9]+)\s?pages'

# BEGIN: MyHTMLParser class (extends HTMLParser) --
class MyHTMLParser(HTMLParser):
    # BEGIN: MyHTMLParser.__init__ (Overrides HTMLParser constructor)
    def __init__(self):
        self.val = 0
        self.gallery = None
        self.gotten = 0
        self.pages = 0
        self.title = ""
        self.s = 0
        self.ext = []
        super(MyHTMLParser, self).__init__()
    # /END: MyHTMLParser.__init__
        
    # BEGIN: MyHTMLParser.handle_starttag
    def handle_starttag(self, tag, attrs):

        # This part of the script looks for the metadata of the page.
        # The doujin name is also stored here.
        # The script fetches it from here as it is easier to fectch.

        if tag == 'meta':
            for attr, value in attrs:
                if attr == 'itemprop' and value == 'name':
                    self.s = 1
                if attr == 'content' and self.s == 1:
                    self.title = value
                    self.s = 0

        # Instead of looking for the 'pages' field in the doujin metadata,
        # the script searches for page thumbnails, counts how many thumbnails
        # are, and also extracts the extension for each page (as some doujins
        # have different extensions for different pages for some reason).
        # NOTE: This behavior might lead to errors, but this is the best I can
        # do right now. I can do better with a better parser.

        if tag == 'img':
            self.val = self.val + 10
            for attr, value in attrs:
                if attr == 'class' and value == 'lazyload':
                    self.val = self.val + 10
                if attr == 'src':
                    self.val = self.val + 10
                    if re.match(gal_match, value):
                        srch = re.search(gal_match, value, re.IGNORECASE)
                        self.gallery = srch.group(1)
                        self.ext.insert(int(srch.group(2))-1, srch.group(3))
                        #print("src: %s" % self.ext)
                if attr == 'data-src':
                    if re.match(gal_match, value):
                        srch = re.search(gal_match, value, re.IGNORECASE)
                        self.val = self.val + 10
                        self.pages = self.pages + 1
                        self.gallery = srch.group(1)
                        self.ext.insert(int(srch.group(2))-1, srch.group(3))
                        self.val = 0
                        #print("data-src: %s\n" % self.gallery)
    # /END: MyHTMLParser.handle_starttag

    # BEGIN: MyHTMLParser.handle_endtag
    def handle_endtag(self, tag):

        # This part of the script handles the doujin page.

        if tag == 'html':
            full_url = "https://i.nhentai.net/galleries/" + self.gallery + "/"
            #print("gallery: %s\nextensions: %s" % (self.gallery, self.ext))
            print("Title: %s\n Pages: %i\n Gallery: %s\nContinue download? [Y/n]:" % (self.title, self.pages, full_url))
            fb = None
            while fb is None:
                fb = sys.stdin.read(1)
                if fb != 'y' and fb != 'Y':
                    print("Aborted!\n")
                    sys.exit()
            print("\n")

            # The script generates a folder with the name of the doujin at your current
            # location (wherever you called the script), if said folder doesn't exist.

            fdir = './' + re.sub('[$.!\\\/\|<>$\?:*]', '_', self.title)
            if not os.path.exists(fdir):
                os.makedirs(fdir)
            fdir = fdir + '/'

            # Loop through each page of the doujin and download them.

            for page in range(1,self.pages+1):
                done = 0
                while done < 1:
                    try:
                        print("Downloading page %i" % (page) + "...")
                        #req2 = urllib.request.Request(url="https://nhentai.net/g/282896/", headers={'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'})
                        with urllib.request.urlopen(full_url + str(page) + "." + self.ext[page-1]) as response, open(fdir + str(page) + "." + self.ext[page-1], 'wb') as outf:
                            shutil.copyfileobj(response, outf)
                        time.sleep(.5)
                        done = 1
                    except urllib.error.HTTPError as e:
                        print("Error: %s\nUrl: %s\nRetrying in 15 seconds... Press Ctrl+C to abort." % (str(e),full_url + str(page) + "." + self.ext[page-1]))
                        time.sleep(15)
                    except urllib.error.URLError as e:
                        print("Error: %s\nUrl: %s\nRetrying in 15 seconds... Press Ctrl+C to abort." % (str(e),full_url + str(page) + "." + self.ext[page-1]))
                        time.sleep(15)
                    except Exception as exc:
                        print("Fatal error: %s\n" % (str(exc)))
                        sys.exit(1)
            print("Done!\n")
    # /END: MyHTMLParser.handle_endtag
            
    # BEGIN: MyHTMLParser.handle_data

    # This part of the code doesn't work at the moment. It was another way of
    # finding the number of pages, but whatever this was looking for back when
    # I first wrote this script, it no longer exists like that.
    # I need to write a proper python DOM handler to use with HTMLparser.

    # HTMLParser sucks.

    # def handle_data(self, data):
        # if re.match(pages_match, data):
            # self.pages = int(re.search(pages_match, data, re.IGNORECASE).group(1))
    # /END: MyHTMLParser.handle_data
# /END: MyHTMLParser class

# BEGIN: MAIN FUNCTION
if __name__ == '__main__':
    print("-- NHentai Doujin Downloader --\nBy Enigmatico (c) 2020\nhttps://gitlab.com/enigmatico/nhentai_downloader\n\n")

    if len(sys.argv) < 2:
        print("You must provide a doujin ID.\n Example: nhentai_download 282896")
    else:
        try:
            print("Downloading: https://nhentai.net/g/" + sys.argv[1] + "/\n")

            req = urllib.request.Request(url="https://nhentai.net/g/" + sys.argv[1] + "/", headers={'User-Agent':' Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0'})
            handler = urllib.request.urlopen(req)
            data = handler.read().decode("utf8")
            handler.close()

            parser = MyHTMLParser()
            parser.feed(data)
        except urllib.error.HTTPError as e:
            print("Error: %s\n" % str(e))
            sys.exit()
        except urllib.error.URLError as e:
            print("Error: %s\n" % str(e))
            sys.exit()
        except Exception as exc:
            print("ERROR: %s\n" % (str(exc)))
            sys.exit()
# /END: MAIN FUNCTION
# /END: PROGRAM
