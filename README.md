# nhentai downloader script

If the torrents are stalled because nobody seeds them, you can use this.

## Requirements:

+ Python 3
+ That's it

## Usage

It runs like any other python scrypt:

```sh
	$ chmod +x nhentai_download.py
	$ ./nhentai_download.py <doujin_id>
```

Where <doujin_id> is the ID of the nhentai doujin (It's in the URL of the doujin).
You can also run it like:

```sh
	$ python3 nhentai_download.py <doujin_id>
```

Or if your python executable is python:

```sh
	$ python nhentai_download.py <doujin_id>
```

It works on Windows and Linux, but won't work with Python2.

If you are on Linux you can also make it a command:

```sh
sudo cp nhentai_download.py /usr/bin/nhentai
sudo chmod +x /usr/bin/nhentai
nhentai <doujin_id>
```
